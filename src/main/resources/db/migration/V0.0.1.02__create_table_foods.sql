create table foods
(
    id          uuid    not null,
    name        varchar not null,
    description varchar not null,
    price       decimal not null,
    primary key (id)
);