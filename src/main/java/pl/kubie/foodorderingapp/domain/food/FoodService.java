package pl.kubie.foodorderingapp.domain.food;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.kubie.foodorderingapp.domain.food.api.FoodDto;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static java.util.stream.Collectors.toMap;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class FoodService {

  private final FoodRepository foodRepository;

  public UUID createFood(FoodDto foodDto) {
    log.info("Creating food {}", foodDto);
    return foodRepository.save(
            new Food(
                foodDto.itemId(),
                foodDto.name(),
                foodDto.itemDescription(),
                foodDto.itemPrice()
            )
        )
        .getId();
  }

  public List<FoodDto> getAllFoods() {
    return foodRepository.findAll()
        .stream()
        .map(Food::toDto)
        .toList();
  }

  public FoodDto getFoodById(UUID itemId) {
    return foodRepository.findById(itemId)
        .map(Food::toDto)
        .orElseThrow();
  }

  @Transactional
  public void updateFood(UUID itemId, FoodDto foodDto) {
    log.info("Updating food {}", foodDto);
    var food = foodRepository.findById(itemId)
        .orElseThrow(() -> new IllegalArgumentException("Food Not found"));
    food.setName(foodDto.name());
    food.setDescription(foodDto.itemDescription());
    food.setPrice(foodDto.itemPrice());
    foodRepository.save(food);
  }

  public void deleteFood(UUID itemId) {
    foodRepository.findById(itemId).ifPresentOrElse(food -> {
          foodRepository.deleteById(itemId);
        },
        () -> {
          throw new IllegalArgumentException("Food not found");
        });
  }


  public List<FoodDto> searchFoods(String searchKey) {
    return foodRepository.findByNameContainingIgnoreCase(searchKey)
        .stream()
        .map(Food::toDto)
        .toList();
  }

  public Map<UUID, FoodDto> findByIdIn(Set<UUID> uuids) {
    return foodRepository.findByIdsIn(uuids)
        .stream()
        .collect(toMap(Food::getId, Food::toDto));
  }

  public Food getById(UUID id) {
    return foodRepository.findById(id).orElseThrow();
  }
}
