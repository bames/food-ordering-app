package pl.kubie.foodorderingapp.domain.order;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.kubie.foodorderingapp.domain.order.api.OrderDto;
import pl.kubie.foodorderingapp.domain.order.api.PlaceOrder;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/orders")
public class OrderController {

  private final OrderService orderService;

  @PostMapping
  public ResponseEntity<OrderDto> placeOrder(@RequestBody PlaceOrder placeOrder) {
    var order = orderService.handle(placeOrder);
    return ResponseEntity.status(HttpStatus.CREATED).body(order);
  }

  @GetMapping("/{orderId}")
  public ResponseEntity<OrderDto> getOrderById(@PathVariable UUID orderId) {
    OrderDto order = orderService.getOrderById(orderId);
    return ResponseEntity.of(Optional.ofNullable(order));
  }

  @GetMapping
  public ResponseEntity<List<OrderDto>> order() {
    List<OrderDto> order = orderService.getAllOrders();
    return ResponseEntity.of(Optional.ofNullable(order));
  }

  @PutMapping("/{orderId}/deliver")
  public ResponseEntity<String> markOrderAsDelivered(@PathVariable UUID orderId) {
    log.info("Marking order {} as delivered", orderId);
    orderService.markAsDelivered(orderId);
    return ResponseEntity.ok(null);
  }
}
