create table orders
(
    id          uuid    not null,
    customer_id uuid    not null,
    order_date  date    not null,
    status      varchar not null,
    primary key (id),
    foreign key (customer_id) references customers (id)
);