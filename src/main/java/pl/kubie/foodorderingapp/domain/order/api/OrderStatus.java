package pl.kubie.foodorderingapp.domain.order.api;

public enum OrderStatus {
  NEW,
  IN_PROGRESS,
  DELIVERED,
  CANCELLED;
}
