package pl.kubie.foodorderingapp.domain.order;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.kubie.foodorderingapp.domain.order.api.OrderDto;
import pl.kubie.foodorderingapp.domain.order.api.OrderStatus;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static jakarta.persistence.CascadeType.ALL;

@Entity
@Table(name = "orders")
@Data
@NoArgsConstructor
class Order {

  @Id
  private UUID id;
  private UUID customerId;
  @OneToMany(cascade = ALL)
  @JoinColumn(name = "order_id")
  private List<OrderPosition> positions;
  private LocalDate orderDate;
  private OrderStatus status;

  public Order(UUID customerId, List<OrderPosition> positions, LocalDate orderDate) {
    this.id = UUID.randomUUID();
    this.customerId = customerId;
    this.positions = positions;
    this.orderDate = orderDate;
    this.status = OrderStatus.NEW;
  }

  public OrderDto toDto() {
    return new OrderDto(
        id,
        customerId,
        positions.stream().map(OrderPosition::toDto).toList(),
        orderTotal(),
        orderDate,
        status
    );
  }

  public BigDecimal orderTotal() {
    return positions.stream()
        .map(OrderPosition::positionValue)
        .reduce(BigDecimal.ZERO, BigDecimal::add);
  }

  public void markAsDelivered() {
    if (status == OrderStatus.IN_PROGRESS) {
      this.status = OrderStatus.DELIVERED;
    } else {
      throw new IllegalStateException("Order cannot be delivered");
    }
  }
}
