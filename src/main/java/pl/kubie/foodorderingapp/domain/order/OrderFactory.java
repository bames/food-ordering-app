package pl.kubie.foodorderingapp.domain.order;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.kubie.foodorderingapp.domain.customer.CustomerService;
import pl.kubie.foodorderingapp.domain.food.FoodService;
import pl.kubie.foodorderingapp.domain.food.api.FoodDto;
import pl.kubie.foodorderingapp.domain.order.api.CreateOrderPosition;
import pl.kubie.foodorderingapp.domain.order.api.PlaceOrder;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Here we have a factory that creates an order.
 * It uses the customer service and the food service.
 */

@Component
@RequiredArgsConstructor
class OrderFactory {

  private final FoodService foodService;
  private final CustomerService customerService;

  public Order createOrder(PlaceOrder command) {
    validateQuantity(command);

    var customer = customerService.getCustomerById(command.customerId());
    var foods = foodService.findByIdIn(command.foodIds());

    return new Order(
        customer.id(),
        createPositions(command, foods),
        LocalDate.now()
    );

  }

  private void validateQuantity(PlaceOrder placeOrder) {
    placeOrder.orderPositions()
        .forEach(position -> {
          if (position.quantity() < 1) {
            throw new RuntimeException("Negative Quantity for Exception");
          }
        });
  }

  private List<OrderPosition> createPositions(PlaceOrder command, Map<UUID, FoodDto> foods) {
    return command.orderPositions()
        .stream()
        .map(position -> createPosition(position, foods.get(position.itemId())))
        .toList();
  }

  private OrderPosition createPosition(CreateOrderPosition command, FoodDto food) {
    return new OrderPosition(
        food.name(),
        food.itemDescription(),
        food.itemPrice(),
        command.quantity()
    );
  }

}
