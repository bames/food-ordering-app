create table customers
(
    id             UUID    not null,
    name           varchar not null,
    address        varchar not null,
    contact_number varchar not null,
    primary key (id)
);