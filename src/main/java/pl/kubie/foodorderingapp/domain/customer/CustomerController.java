package pl.kubie.foodorderingapp.domain.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.kubie.foodorderingapp.domain.customer.api.CustomerDto;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(path = "/customers")
public class CustomerController {
  private final CustomerService customerService;

  @Autowired
  public CustomerController(CustomerService customerService) {
    this.customerService = customerService;
  }

  @PostMapping
  public ResponseEntity<String> createCustomer(@RequestBody CustomerDto customerDto) {
    customerService.createCustomer(customerDto);
    return ResponseEntity
        .status(HttpStatus.CREATED)
        .body("Customer created successfully");
  }

  @GetMapping("/{customerId}")
  public ResponseEntity<CustomerDto> getCustomerById(@PathVariable UUID customerId) {
    CustomerDto customer = customerService.getCustomerById(customerId);
    return ResponseEntity.ok(customer);
  }

  @GetMapping
  public ResponseEntity<List<CustomerDto>> getAllCustomers() {
    List<CustomerDto> customers = customerService.getAllCustomers();
    return ResponseEntity.ok(customers);
  }

  @PutMapping("/{customerId}")
  public ResponseEntity<String> updateCustomer(@PathVariable UUID customerId, @RequestBody CustomerDto customerDto) {
    customerService.updateCustomer(customerId, customerDto);
    return ResponseEntity.ok("Customer updated successfully");
  }

  @DeleteMapping("/{customerId}")
  public ResponseEntity<String> deleteCustomer(@PathVariable UUID customerId) {
    customerService.deleteCustomer(customerId);
    return ResponseEntity.ok("Customer deleted successfully");
  }

}
