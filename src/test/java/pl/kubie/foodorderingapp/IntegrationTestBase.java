package pl.kubie.foodorderingapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.kubie.foodorderingapp.domain.customer.CustomerService;
import pl.kubie.foodorderingapp.domain.food.FoodService;
import pl.kubie.foodorderingapp.domain.order.OrderService;

@SpringBootTest
public class IntegrationTestBase {

  @Autowired
  OrderService orderService;
  @Autowired
  FoodService foodService;
  @Autowired
  CustomerService customerService;
}
