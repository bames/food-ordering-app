package pl.kubie.foodorderingapp;

import pl.kubie.foodorderingapp.domain.customer.api.CustomerDto;

import java.util.UUID;

public class TestCustomers {

  public static CustomerDto.CustomerDtoBuilder someCustomer() {
    return CustomerDto.builder()
        .id(UUID.randomUUID())
        .name("John")
        .address("New York")
        .contactNumber("09998008");
  }
}
