package pl.kubie.foodorderingapp;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import pl.kubie.foodorderingapp.domain.customer.CustomerService;
import pl.kubie.foodorderingapp.domain.customer.api.CustomerDto;
import pl.kubie.foodorderingapp.domain.food.FoodService;
import pl.kubie.foodorderingapp.domain.food.api.FoodDto;
import pl.kubie.foodorderingapp.domain.order.OrderService;
import pl.kubie.foodorderingapp.domain.order.api.CreateOrderPosition;
import pl.kubie.foodorderingapp.domain.order.api.PlaceOrder;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@SpringBootApplication
public class FoodOrderingAppApplication {

  public static void main(String[] args) {
    SpringApplication.run(FoodOrderingAppApplication.class, args);
  }

  @Bean
  CommandLineRunner sampleOrder(
      CustomerService customerService,
      FoodService foodService,
      OrderService orderService
  ) {
    return args -> {

      UUID customerId = UUID.fromString("6f29dc47-8e3b-4926-ae64-2bfa2ea4027f");
      UUID burgerId = UUID.randomUUID();

      customerService.createCustomer(
          new CustomerDto(
              customerId,
              "Charles",
              "Muhaisnah ",
              "9997009987"
          )
      );


      foodService.createFood(
          new FoodDto(
              burgerId,
              "Burger",
              "Delicious Burger",
              new BigDecimal("123.00")
          )
      );

      orderService.handle(new PlaceOrder(
          customerId,
          List.of(new CreateOrderPosition(burgerId, 1))
      ));
    };

  }

  @Bean
  CommandLineRunner sampleCustomer(CustomerService customerService) {
    return args -> {
      customerService.createCustomer(
          new CustomerDto(
              UUID.randomUUID(),
              "Elias",
              "Dubai Hills",
              "989935739")
      );
      customerService.createCustomer(
          new CustomerDto(
              UUID.randomUUID(),
              "Abby",
              "Dubai Emaar",
              "08304830483")
      );
    };
  }

  @Bean
  CommandLineRunner sampleFood(FoodService foodService) {
    return args -> {
      foodService.createFood(
          new FoodDto(
              UUID.randomUUID(),
              "Chicken Burger",
              "Chicken & Cheese Burger with Lettuce",
              new BigDecimal("123.456")
          )
      );
      foodService.createFood(
          new FoodDto(
              UUID.randomUUID(),
              "Beef Burger",
              "Beef & Cheese Burger with Lettuce & Tomato",
              new BigDecimal("231.456")
          )
      );
    };
  }
}
