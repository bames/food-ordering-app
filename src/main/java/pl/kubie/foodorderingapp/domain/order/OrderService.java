package pl.kubie.foodorderingapp.domain.order;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.kubie.foodorderingapp.domain.order.api.PlaceOrder;
import pl.kubie.foodorderingapp.domain.order.api.OrderDto;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class OrderService {

  private final OrderFactory orderFactory;
  private final OrderRepository orderRepository;

  public OrderDto handle(PlaceOrder command) {
    log.info("Creating order {}", command);
    var order = orderFactory.createOrder(command);
    orderRepository.save(order);
    log.info("Order created successfully {}", order);
    return order.toDto();
  }

  public List<OrderDto> getAllOrders() {
    return orderRepository.findAll()
        .stream()
        .map(Order::toDto)
        .toList();
  }

  public OrderDto getOrderById(UUID orderId) {
    return orderRepository.findById(orderId)
        .map(Order::toDto)
        .orElseThrow();
  }

  public void markAsDelivered(UUID orderId) {
    Order order = orderRepository.findById(orderId)
        .orElseThrow(() -> new NoSuchElementException("Order not found"));
    order.markAsDelivered();
    orderRepository.save(order);
  }

}
