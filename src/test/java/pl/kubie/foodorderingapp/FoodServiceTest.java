package pl.kubie.foodorderingapp;


import org.junit.jupiter.api.Test;
import pl.kubie.foodorderingapp.domain.food.api.FoodDto;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static pl.kubie.foodorderingapp.TestFoods.someFood;

public class FoodServiceTest extends IntegrationTestBase {

  @Test
  void shouldCreateNewFood() {
    // given
    var newFood = someFood("30");

    // when
    foodService.createFood(newFood);

    // then
    var foods = foodService.getAllFoods();
    assertThat(foods).hasSizeGreaterThan(0);
    // assertThat(food.get(0))
    assertThat(findFoodWithId(foods, newFood))
        .satisfies(
            createdFood -> {
              assertThat(createdFood.itemId()).isEqualTo(newFood.itemId());
              assertThat(createdFood.name()).isEqualTo(newFood.name());
              assertThat(createdFood.itemDescription()).isEqualTo(newFood.itemDescription());
              assertThat(createdFood.itemPrice()).isEqualTo(newFood.itemPrice());
            }
        );
  }

  @Test
  void shouldRetrieveFoodById() {
    // Given
    var newFood = someFood("30");
    foodService.createFood(newFood);

    // When
    var retrievedFood = foodService.getFoodById(newFood.itemId());

    //then
    assertThat(retrievedFood).isNotNull();
    assertThat(retrievedFood.itemId()).isEqualTo(newFood.itemId());
    assertThat(retrievedFood.name()).isEqualTo(newFood.name());
    assertThat(retrievedFood.itemDescription()).isEqualTo(newFood.itemDescription());
    assertThat(retrievedFood.itemPrice()).isEqualTo(newFood.itemPrice());

  }

  private static FoodDto findFoodWithId(List<FoodDto> foodList, FoodDto newFood) {
    return foodList.stream()
        .filter(food -> food.itemId().equals(newFood.itemId()))
        .findFirst()
        .orElseThrow();
  }

  @Test
  void shouldUpdateFood() {
    // Given
    var newFood = someFood("30");
    foodService.createFood(newFood);

    // When
    var updatedFood = new FoodDto(
        newFood.itemId(),
        "Updated Pizza Name (Margherita)",
        "Margherita Pizza with lot of cheese",
        new BigDecimal("30")
    );
    foodService.updateFood(newFood.itemId(), updatedFood);

    //Then
    var retrievedFood = foodService.getFoodById(newFood.itemId());
    System.out.println("Actual value: " + retrievedFood.itemPrice());

    assertThat(retrievedFood).isNotNull();
    assertThat(retrievedFood.itemId()).isEqualTo(updatedFood.itemId());
    assertThat(retrievedFood.name()).isEqualTo(updatedFood.name());
    assertThat(retrievedFood.itemDescription()).isEqualTo(updatedFood.itemDescription());
    assertThat(retrievedFood.itemPrice()).isEqualTo(updatedFood.itemPrice());
  }

}

