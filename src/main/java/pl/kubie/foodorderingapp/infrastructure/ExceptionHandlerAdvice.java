package pl.kubie.foodorderingapp.infrastructure;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.NoSuchElementException;

@Slf4j
@ControllerAdvice
public class ExceptionHandlerAdvice {

  @ExceptionHandler({RuntimeException.class})
  public ResponseEntity<?> handleRuntimeException(RuntimeException exception) {
    log.error("Operation failed. Reason: {}", exception.getMessage());
    return ResponseEntity
        .status(500)
        .body("Operation failed. Reason: %s".formatted(exception.getMessage()));
  }

  @ExceptionHandler({NoSuchElementException.class})
  public ResponseEntity<?> handleNoSuchElementException(NoSuchElementException exception) {
    log.error("Operation failed. Reason: {}", exception.getMessage());
    return ResponseEntity
        .status(404)
        .body("Element not found. Reason: %s".formatted(exception.getMessage()));
  }

  @ExceptionHandler({IllegalArgumentException.class})
  public ResponseEntity<?> handleIllegalArgumentException(IllegalArgumentException exception) {
    log.error("Operation failed. Reason: {}", exception.getMessage());
    return ResponseEntity
        .status(400)
        .body("Bad request - Illegal Argument. Reason: %s".formatted(exception.getMessage()));
  }


}
