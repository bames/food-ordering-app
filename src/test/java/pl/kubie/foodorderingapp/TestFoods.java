package pl.kubie.foodorderingapp;

import pl.kubie.foodorderingapp.domain.food.api.FoodDto;

import java.math.BigDecimal;
import java.util.UUID;

public class TestFoods {

  public static FoodDto someFood(String price) {
    return new FoodDto(
        UUID.randomUUID(),
        "Pizza",
        "Cheesy Pizza with Zaatar",
        new BigDecimal(price)
    );
  }
}
