package pl.kubie.foodorderingapp.domain.order.api;

import java.math.BigDecimal;
import java.util.UUID;

public record OrderPositionDto(
    UUID id,
    String name,
    String description,
    BigDecimal price,
    Integer quantity
) {

}
