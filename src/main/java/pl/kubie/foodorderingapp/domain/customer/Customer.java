package pl.kubie.foodorderingapp.domain.customer;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.kubie.foodorderingapp.domain.customer.api.CustomerDto;

import java.util.UUID;

@Entity
@Table(name = "customers")
@Data
@AllArgsConstructor
@NoArgsConstructor
class Customer {

  @Id
  private UUID id;
  private String name;
  private String address;
  private String contactNumber;

  public CustomerDto toDto() {
    return new CustomerDto(id, name, address, contactNumber);
  }
}
