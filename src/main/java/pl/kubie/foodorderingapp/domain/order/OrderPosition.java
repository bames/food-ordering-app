package pl.kubie.foodorderingapp.domain.order;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.kubie.foodorderingapp.domain.order.api.OrderPositionDto;

import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Table(name = "order_positions")
@Data
@NoArgsConstructor
class OrderPosition {

  @Id
  private UUID id;
  private String name;
  private String description;
  private BigDecimal price;
  private int quantity;

  public OrderPosition(String name, String description, BigDecimal price, int quantity) {
    this.id = UUID.randomUUID();
    this.name = name;
    this.description = description;
    this.price = price;
    this.quantity = quantity;
  }

  public BigDecimal positionValue() {
    return price.multiply(BigDecimal.valueOf(quantity));
  }

  public OrderPositionDto toDto() {
    return new OrderPositionDto(
        id,
        name,
        description,
        price,
        quantity
    );
  }
}

