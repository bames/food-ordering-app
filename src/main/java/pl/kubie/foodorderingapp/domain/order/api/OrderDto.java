package pl.kubie.foodorderingapp.domain.order.api;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public record OrderDto(
    UUID orderId,
    UUID customerId,
    List<OrderPositionDto> positions,
    BigDecimal orderAmount,
    LocalDate orderDate,
    OrderStatus status
) {

}
