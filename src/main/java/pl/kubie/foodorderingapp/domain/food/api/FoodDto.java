package pl.kubie.foodorderingapp.domain.food.api;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.math.BigDecimal;
import java.util.UUID;


public record FoodDto(
        @NotNull UUID itemId,
        @NotBlank String name,
        @NotBlank String itemDescription,
        @NotNull BigDecimal itemPrice

){
}