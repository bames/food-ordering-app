package pl.kubie.foodorderingapp.domain.food;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.kubie.foodorderingapp.domain.food.api.FoodDto;

import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Table(name = "foods")
@Data
@AllArgsConstructor
@NoArgsConstructor
class Food {

  @Id
  private UUID id;
  private String name;
  private String description;
  private BigDecimal price;

  public FoodDto toDto() {
    return new FoodDto(id, name, description, price);
  }
}
