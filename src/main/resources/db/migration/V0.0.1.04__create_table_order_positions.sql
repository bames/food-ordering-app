create table order_positions
(
    id          uuid    not null,
    order_id    uuid,
    name        varchar not null,
    description varchar not null,
    price       decimal not null,
    quantity    int     not null,
    primary key (id),
    foreign key (order_id) references orders (id)
);