package pl.kubie.foodorderingapp.domain.order.api;


import java.util.UUID;

public record CreateOrderPosition(UUID itemId, int quantity) {

}
