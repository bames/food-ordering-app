package pl.kubie.foodorderingapp.domain.order.api;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public record PlaceOrder(UUID customerId, List<CreateOrderPosition> orderPositions) {

  public Set<UUID> foodIds() {
    return orderPositions
        .stream()
        .map(CreateOrderPosition::itemId)
        .collect(Collectors.toSet());
  }

}
