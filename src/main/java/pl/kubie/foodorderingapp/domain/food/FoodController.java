
package pl.kubie.foodorderingapp.domain.food;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.kubie.foodorderingapp.domain.food.api.FoodDto;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(path = "/foods")
public class FoodController {
  private final FoodService foodService;

  @Autowired
  public FoodController(FoodService foodService) {
    this.foodService = foodService;
  }

  @GetMapping
  public ResponseEntity<List<FoodDto>> getAllFoods() {
    List<FoodDto> foods = foodService.getAllFoods();
    return ResponseEntity.ok(foods);
  }

  @GetMapping("/{foodId}")
  public ResponseEntity<FoodDto> getFoodById(@PathVariable UUID foodId) {
    FoodDto food = foodService.getFoodById(foodId);
    if (food != null) {
      return ResponseEntity.ok(food);
    } else {
      return ResponseEntity.notFound().build();
    }
  }


  @PostMapping
  public ResponseEntity<String> createFood(@Valid @RequestBody FoodDto foodDto) {
    foodService.createFood(foodDto);
    return ResponseEntity.status(HttpStatus.CREATED).body("Food item created successfully");
  }

  @PutMapping("/{foodId}")
  public ResponseEntity<String> updateFood(@PathVariable UUID foodId, @Valid @RequestBody FoodDto foodDto) {
    foodService.updateFood(foodId, foodDto);
    return ResponseEntity.ok("Food item updated successfully");
  }

  @DeleteMapping("/{foodId}")
  public ResponseEntity<String> deleteFood(@PathVariable UUID foodId) {
    foodService.deleteFood(foodId);
    return ResponseEntity.ok("Food item deleted successfully");
  }

  @GetMapping("/menu")
  public ResponseEntity<List<FoodDto>> getMenu() {
    List<FoodDto> menuItems = foodService.getAllFoods();
    return ResponseEntity.ok(menuItems);
  }

  @GetMapping("/search")
  public ResponseEntity<List<FoodDto>> searchFoods(@RequestParam(required = false) String searchKey) {
    List<FoodDto> searchResults = foodService.searchFoods(searchKey);
    return ResponseEntity.ok(searchResults);
  }
}





