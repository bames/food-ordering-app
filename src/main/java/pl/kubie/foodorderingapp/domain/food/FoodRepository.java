package pl.kubie.foodorderingapp.domain.food;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Repository
interface FoodRepository extends JpaRepository<Food, UUID> {

  @Query("SELECT f FROM Food f WHERE f.name = ?1")
  List<Food> findByNameContainingIgnoreCase(String searchKey);

  @Query("""
      SELECT f
      FROM Food f
      WHERE f.id in :ids
      """
  )
  Set<Food> findByIdsIn(Set<UUID> ids);

}

