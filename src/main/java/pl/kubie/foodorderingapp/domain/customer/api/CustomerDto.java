package pl.kubie.foodorderingapp.domain.customer.api;

import lombok.Builder;

import java.util.UUID;

@Builder
public record CustomerDto(
    UUID id,
    String name,
    String address,
    String contactNumber
) {
}

