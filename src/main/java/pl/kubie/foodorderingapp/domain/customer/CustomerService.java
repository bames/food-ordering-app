package pl.kubie.foodorderingapp.domain.customer;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.kubie.foodorderingapp.domain.customer.api.CustomerDto;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class CustomerService {

  private final CustomerRepository customerRepository;

  public UUID createCustomer(CustomerDto customerDto) {
    log.info("Creating customer {}", customerDto);
    return customerRepository.save(
            new Customer(
                customerDto.id(),
                customerDto.name(),
                customerDto.address(),
                customerDto.contactNumber()
            )
        )
        .getId();
  }

  public List<CustomerDto> getAllCustomers() {
    return customerRepository.findAll()
        .stream()
        .map(Customer::toDto)
        .toList();
  }

  public CustomerDto getCustomerById(UUID customerId) {
    return customerRepository.findById(customerId)
        .map(Customer::toDto)
        .orElseThrow();

  }

  public void updateCustomer(UUID customerId, CustomerDto customerDto) {
    var customer = customerRepository.findById(customerId)
        .orElseThrow(() -> new IllegalArgumentException("Customer not found"));
    customer.setName(customerDto.name());
    customer.setAddress(customerDto.address());
    customer.setContactNumber(customerDto.contactNumber());
    customerRepository.save(customer);
  }

  public void deleteCustomer(UUID customerId) {
    Optional<Customer> customerOptional = customerRepository.findById(customerId);
    customerOptional.ifPresentOrElse(
        customer -> customerRepository.deleteById(customerId),
        () -> {
          throw new IllegalArgumentException("Customer not found");
        });
  }

}
