package pl.kubie.foodorderingapp;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import pl.kubie.foodorderingapp.domain.order.api.OrderDto;
import pl.kubie.foodorderingapp.domain.order.api.PlaceOrder;
import pl.kubie.foodorderingapp.domain.order.api.CreateOrderPosition;

import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static pl.kubie.foodorderingapp.domain.order.api.OrderStatus.NEW;

public class OrderServiceTest extends IntegrationTestBase {

  @Test
  void shouldCreateOrder() {
    // given
    var foodId = someFood("30.00");
    var customerId = someCustomer();

    var command = new PlaceOrder(
        customerId,
        List.of(
            new CreateOrderPosition(foodId, 2),
            new CreateOrderPosition(foodId, 3)
        )
    );

    // when
    OrderDto order = orderService.handle(command);

    // then
    assertThat(order.orderAmount())
        .usingComparator(BigDecimal::compareTo)
        .isEqualTo(new BigDecimal("150.00"));
    assertThat(order.status()).isEqualTo(NEW);

  }

  @Test
  void shouldThrowExceptionWhenCustomerDoesNotExist() {
    // given
    var nonExistingCustomer = UUID.randomUUID();
    var food = someFood("30");
    var command = new PlaceOrder(nonExistingCustomer, List.of(new CreateOrderPosition(food, 1)));
    // expect
    assertThrows(NoSuchElementException.class, () -> orderService.handle(command));
  }

  // todo make this test pass
  @Test
  @Disabled
  void shouldThrowExceptionWhenOrderPositionsIsEmpty() {
    // given
    var customer = someCustomer();
    var command = new PlaceOrder(customer, List.of());
    // expect
    assertThrows(IllegalArgumentException.class, () -> orderService.handle(command));
  }

  private UUID someCustomer() {
    return customerService.createCustomer(TestCustomers.someCustomer().build());
  }

  private UUID someFood(String price) {
    return foodService.createFood(TestFoods.someFood(price));
  }
}
