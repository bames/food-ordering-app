package pl.kubie.foodorderingapp;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static pl.kubie.foodorderingapp.TestCustomers.*;

public class CustomerServiceTest extends IntegrationTestBase {

  @Test
  void shouldCreateNewCustomer() {
    // given
    var newCustomer = someCustomer().build();

    // when
    var customerId = customerService.createCustomer(newCustomer);

    // then
    assertThat(customerService.getCustomerById(customerId))
        .satisfies(
            customer -> {
              assertThat(customer.id()).isEqualTo(newCustomer.id());
              assertThat(customer.name()).isEqualTo(newCustomer.name());
              assertThat(customer.address()).isEqualTo(newCustomer.address());
              assertThat(customer.contactNumber()).isEqualTo(newCustomer.contactNumber());
            }
        );
  }

  @Test
  void shouldUpdateCustomer() {
      // given
      var existingCustomer = someCustomer().build();
      var updatedCustomer = someCustomer().name("Ronaldo`").address("Dubai Hills").build();

      // Create the initial customer
      var customerId = customerService.createCustomer(existingCustomer);

      // when
      customerService.updateCustomer(customerId, updatedCustomer);

      // then
      assertThat(customerService.getCustomerById(customerId))
              .satisfies(
                      customer -> {
                          assertThat(customer.id()).isEqualTo(existingCustomer.id());
                          assertThat(customer.name()).isEqualTo(updatedCustomer.name());
                          assertThat(customer.address()).isEqualTo(updatedCustomer.address());
                          assertThat(customer.contactNumber()).isEqualTo(existingCustomer.contactNumber());
                      }
              );
  }


}
